<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title }}</title>
    <link rel="stylesheet" href="./css/pdf.css">
    <style>
        table{
            table-layout: fixed;
        }

        td {
            word-wrap:break-word;
        }
    </style>
</head>
<body>
    <div class="section">
        <div class="container">
            <div class="container">
                @yield('content');
            </div>
        </div>
    </div>
</body>
</html>
