@extends('layouts.pdf')

@section('content')
    <center>
        <h2 class="title has-text-dark is-size-4">{{$title}}</h2>
        <h2 class="subtitle has-text-dark is-size-4">Sucursal: {{ Auth::user()->branch->name }}</h2>
    </center>
    <div class="columns has-margin-top-5">
        <div class="column is-12">
            <table class="table is-fullwidth">
                <tr>
                    <th><center class="is-size-6">FECHA DE IMPRESIÓN: {{ date('d/m/Y') }}</center></th>
                    <th><center class="is-size-6">HORA DE IMPRESIÓN: {{ date('h:i:s a') }}</center></th>
                </tr>
            </table>
        </div>
    </div>
    <div class="columns has-margin-top-0">
        <div class="column is-12">
            <table class="table is-bordered is-striped is-narrow is hoverable is-fullwidth">
                <thead>
                    <tr>
                        <th style="width: 15%">CÓDIGO</th>
                        <th style="width: 45%;">NOMBRE</th>
                        <th style="width: 20%">MARCA</th>
                        <th style="width: 10%">U.M</th>
                        <th style="width: 10%">STOCK</th>
                    </tr>
                </thead>
                <thead>
                    @foreach($products as $product)
                        <tr>
                            <td>{{ $product->code }}</td>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->brand }}</td>
                            <td>{{ $product->measure_unit }}</td>
                            <td>{{ $product->in_stock }}</td>
                        </tr>
                    @endforeach
                </thead>
            </table>
        </div>
    </div>
@endsection
