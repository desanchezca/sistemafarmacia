@extends('layouts.pdf')

@section('content')
    <center>
        <h2 class="title has-text-dark is-size-4">{{$title}}</h2>
        <h5 class="subtitle is-size-6">Desde: 21/01/2019 Hasta: 12/12/2019</h5>
    </center>
    <div class="columns has-margin-top-5">
        <div class="column is-12">
            <table class="table is-fullwidth">
                <tr>
                    <th><center><p class="is-size-6">Fecha de impresión: 10/15/2019</p></center></th>
                    <th><center><p class="is-size-6">Hora de impresión: 10/15/2019</p></center></th>
                </tr>
            </table>
        </div>
    </div>
    <div class="columns has-margin-top-0">
        <div class="column is-12">
            <table class="table is-bordered is-striped is-narrow is hoverable is-fullwidth">
                <thead>
                <tr>
                    <th class="is-size-7" style="width: 40%">VENDEDOR</th>
                    <th class="is-size-7" style="width: 15%">Nº VENTAS</th>
                    <th class="is-size-7" style="width: 15%">MON. MIN. (S/)</th>
                    <th class="is-size-7" style="width: 15%">MON. MAX. (S/)</th>
                    <th class="is-size-7" style="width: 15%">MON. PROM. (S/)</th>
                </tr>
                </thead>
                <thead>
                <tr>
                    <td class="is-size-7">DENIS JOSELI SANCHEZ CARRANZA</td>
                    <td class="is-size-7">350</td>
                    <td class="is-size-7">15.50</td>
                    <td class="is-size-7">350.50</td>
                    <td class="is-size-7">150.50</td>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
