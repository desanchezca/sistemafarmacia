@extends('layouts.pdf')

@section('content')
    <center>
        <h2 class="title has-text-dark is-size-4">{{$title}}</h2>
        <h5 class="subtitle is-size-6">DESDE: {{ date_format($from, 'd/m/Y') }} HASTA: {{ date_format($to, 'd/m/Y') }}</h5>
    </center>
    <div class="columns has-margin-top-5">
        <div class="column is-12">
            <table class="table is-fullwidth">
                <tr>
                    <th><center class="is-size-6">FECHA DE IMPRESIÓN: {{ date('d/m/Y') }}</center></th>
                    <th><center class="is-size-6">HORA DE IMPRESIÓN: {{ date('h:i:s a') }}</center></th>
                </tr>
            </table>
        </div>
    </div>
    <div class="columns has-margin-top-0">
        <div class="column is-12">
            <table class="table is-bordered is-striped is-narrow is hoverable is-fullwidth">
                <thead>
                <tr>
                    <th class="is-size-7" style="width: 5%">DÍA</th>
                    <th class="is-size-7" style="width: 20%">DOCUMENTO</th>
                    <th class="is-size-7" style="width: 45%">PROVEEDOR</th>
                    <th class="is-size-7" style="width: 10%">TOTAL</th>
                </tr>
                </thead>
                <thead>
                    @foreach($purchases as $purchase)
                        <tr>
                            <td class="is-size-7">{{ date_format($purchase->created_at, 'd') }}</td>
                            <td class="is-size-7">{{ $purchase->code }}</td>
                            <td class="is-size-7">{{ $purchase->provider_name }}</td>
                            <td class="is-size-7">{{ $purchase->total }}</td>
                        </tr>
                    @endforeach
                </thead>
            </table>
        </div>
    </div>
@endsection
