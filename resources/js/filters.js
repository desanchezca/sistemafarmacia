import Vue from 'vue'

Vue.filter('twoDecimals', function (value) {
    return parseFloat(value).toFixed(2)
})

Vue.filter('toPEN', function (value) {
    return `S/ ${parseFloat(value).toFixed(2)}`;
});
