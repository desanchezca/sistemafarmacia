export const mutation = {
    setSales(state, sales) {
        state.sales = sales;
    },
    addSale(state, sale) {
        let sales = state.sales;
        sales.push(sale);
        state.sales = sales;
    }
};
