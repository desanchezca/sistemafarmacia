export const actions = {
    getSales(context) {
        axios.get('/api/sale-documents/')
            .then((response) => {
                let sales = response.data.data;
                context.commit('setSales', sales);
            });
    },
    addSale(context, sale) {
        axios.post('/api/sale-documents', sale)
            .then((response) => {
                let sale = response.data.data;
                context.commit('addSale', sale);
            })
    }
};
