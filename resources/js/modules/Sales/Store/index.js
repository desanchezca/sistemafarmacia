import {getters} from './getters.js'
import {actions} from './actions.js'
import {mutations} from './mutations.js'

const state = {
    sales: []
};

const namespaced = true;

export const sales = {
    namespaced,
    state,
    getters,
    actions,
    mutations
};
