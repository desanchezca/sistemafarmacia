export const actions = {
    getCategories(context) {
        axios.get('/api/categories')
            .then((response) => {
                let categories = response.data;
                context.commit('setCategories', categories);
            });
    }
}
