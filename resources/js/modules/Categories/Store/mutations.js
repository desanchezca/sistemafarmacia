export const mutations = {
    setCategories(state, categories) {
        state.categories = categories;
    },
    addCategory(state, category) {
        let categories = state.categories;
        categories.push(category);
        state.categories = categories;
    }
}
