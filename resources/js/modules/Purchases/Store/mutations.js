export const mutations = {
    setPurchases(state, purchases) {
        state.purchases = purchases;
    },
    addPurchase(state, purchase) {
        let purchases = state.purchases;
        purchases.push(purchase);
        state.purchases = purchases;
    }
};
