import {getters} from './getters.js'
import {actions} from './actions.js'
import {mutations} from './mutations.js'

const state = {
    purchases: [],
};


const namespaced = true;

export const purchases = {
    namespaced,
    state,
    getters,
    actions,
    mutations
};
