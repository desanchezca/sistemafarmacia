export const actions = {
    getPurchases(context) {
        axios.get('/api/purchase-documents/')
            .then((response) => {
                let purchases = response.data.data;
                context.commit('setPurchases', purchases);
            });
    },
    addPurchase(context, purchase) {
        axios.post('/api/purchase-documents/', purchase)
            .then((response) => {
                let purchase = response.data.data;
                context.commit('addPurchase', purchase);
            });
    }
};
