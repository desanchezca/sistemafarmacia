export const getters = {
    completed: state => {
        return state.purchases.filter(
            purchase => purchase.state === 'COMPLETADO'
        );
    },
    getPurchaseById: state => (id) => {
        return state.purchases.find(
            purchase => purchase.id === id
        );
    }
};
