import {getters} from './getters'
import {actions} from './actions'
import {mutations} from './mutations'

const state = {
    laboratiores: []
}

const namespaced = true;

export const laboratories = {
    namespaced,
    state,
    getters,
    actions,
    mutations
}
