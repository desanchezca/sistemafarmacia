export const mutations = {
    setLaboratories(state, laboratories) {
        state.laboratories = laboratories;
    }
}
