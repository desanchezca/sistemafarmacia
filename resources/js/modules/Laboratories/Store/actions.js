export const actions = {
    getLaboratories(context) {
        axios.get('/api/laboratories')
            .then((response) => {
                let laboratories = response.data;
                context.commit('setLaboratories', laboratories);
            });
    }
}
