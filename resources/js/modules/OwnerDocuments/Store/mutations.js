export const mutations = {
    setOwnerDocuments(state, ownerDocuments){
        state.ownerDocuments = ownerDocuments;
    },
    addOwnerDocument(state, ownerDocument) {
        let ownerDocuments = state.ownerDocuments;
        ownerDocuments.push(ownerDocument);
        state.ownerDocument = ownerDocuments;
    }
}
