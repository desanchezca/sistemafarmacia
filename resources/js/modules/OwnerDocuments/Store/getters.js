export const getters = {
    getOwnerDocumentByIndentityDocument: state => (identityDocument) => {
        return state.ownerDocuments.find(
            ownerDocument => ownerDocument.identity_document = identityDocument
        )
    }
}
