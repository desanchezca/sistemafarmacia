export const actions = {
    getOwnerDocuments(context) {
        axios.get('/api/owner-documents')
            .then((response) => {
                let ownerDocuments = response.data.data;
                context.commit('setOwnerDocuments', ownerDocuments);
            });
    },
    addOwnerDocument(context, ownerDocument) {
        axios.post('/api/owner-documents', ownerDocument)
            .then((response) => {
                let ownerDocument = response.data.data;
                context.commit('addOwnerDocument', ownerDocument);
            });
    }
}
