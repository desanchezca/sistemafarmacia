import {actions} from './actions'
import {getters} from './getters'
import {mutations} from './mutations'

const state = {
    ownerDocuments: []
}

const namespaced = true;

export const ownerDocuments = {
    namespaced,
    state,
    actions,
    getters,
    mutations
}
