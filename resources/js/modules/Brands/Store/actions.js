export const actions = {
    getBrands(context) {
        axios.get('/api/brands')
            .then((response) => {
                let brands = response.data;
                context.commit('setBrands', brands);
            });
    }
}
