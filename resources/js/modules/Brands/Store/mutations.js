export const mutations = {
    setBrands(state, brands) {
        state.brands = brands;
    },
    addBrand(state, brand) {
        let brands = state.brands;
        brands.push(brand);
        state.brands = brands;
    }
}
