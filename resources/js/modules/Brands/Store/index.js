import {getters} from './getters'
import {actions} from './actions'
import {mutations} from './mutations'


const state = {
    brands: []
}

const namespaced = true;

export const brands = {
    namespaced,
    state,
    getters,
    actions,
    mutations
}
