export const mutations = {
    setProducts(state, products) {
        state.products = products;
    },
    addProduct(state, product) {
        let products = state.products;
        products.push(product);
        state.products = products;
    }
};
