export const actions = {
    getProducts(context) {
        axios.get('/api/products')
            .then((response) => {
                let products = response.data.data;
                let meta = response.data.meta;
                localStorage.setItem('user', meta.user);
                localStorage.setItem('branch', meta.branch);
                context.commit('setProducts', products);
            });
    },
    /*addProduct(context, product) {
        axios.post('/api/products', product)
            .then((response) => {
                let product = response.data.data;
                context.commit('addProduct', product);
            });
    }*/
};
