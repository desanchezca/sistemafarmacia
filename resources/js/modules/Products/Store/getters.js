export const getters = {
    getProductById: state => (id) => {
        return state.products.find(
            product => product.id === id
        );
    },
    getProductByCode: state => (code) => {
        return state.products.find(
            product => product.code === code
        );
    }
}
