export const mutations = {
    setMeasureUnits(state, measureUnits) {
        state.measureUnits = measureUnits;
    },
    addMeasureUnit(state, measureUnit) {
        let measureUnits = state.measureUnit;
        measureUnits.push(measureUnit);
        state.measureUnits = measureUnits;
    }
}
