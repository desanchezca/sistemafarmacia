export const actions = {
    getMeasureUnits(context) {
        axios.get('/api/measure-units')
            .then((response) => {
                let measureUnits = response.data;
                context.commit('setMeasureUnits', measureUnits);
            });
    }/*,
    addMeasureUnit(context, measureUnit) {
        axios.post('/api/measure-units')
            .then((response) => {
                let measureUnit = response.data.data;
                context.commit('addMeasureUnit', measureUnit);
            });
    }*/
}
