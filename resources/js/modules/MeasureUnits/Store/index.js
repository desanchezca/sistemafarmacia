import {actions} from './actions'
import {getters} from './getters'
import {mutations} from './mutations'

const state = {
    measureUnits: []
};

const namespaced = true;

export const measureUnits = {
    namespaced,
    state,
    actions,
    getters,
    mutations
}
