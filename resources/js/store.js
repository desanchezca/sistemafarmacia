import Vue from 'vue';
import Vuex from 'vuex';
import {purchases} from "./modules/Purchases/Store";
import {products} from "./modules/Products/Store";
import {categories} from "./modules/Categories/Store";
import {brands} from "./modules/Brands/Store";
import {laboratories} from "./modules/Laboratories/Store";
import {ownerDocuments} from "./modules/OwnerDocuments/Store";
import {measureUnits} from "./modules/MeasureUnits/Store";

Vue.use(Vuex);

export const store = new Vuex.Store({
    modules: {
        purchases: purchases,
        products: products,
        categories: categories,
        brands: brands,
        laboratories: laboratories,
        ownerDocuments: ownerDocuments,
        measureUnits: measureUnits
    }
});
