require('./bootstrap');

import Vue from 'vue'

import App from './App.vue'
import {store} from './store'
import router from './router.js'
import vuelidate from 'vuelidate'
import VueToast from 'vue-toast-notification'
import VModal from 'vue-js-modal'
import VueSpinners from 'vue-spinners'

import './filters'
import 'bulma/css/bulma.min.css'
import 'bulma-helpers/css/bulma-helpers.min.css'
import '@fortawesome/fontawesome-free/css/all.min.css'
import 'vue-search-select/dist/VueSearchSelect.css'
import 'vue-toast-notification/dist/index.css';
import 'vue2-daterange-picker/dist/vue2-daterange-picker.css'

Vue.use(vuelidate);
Vue.use(VueToast);
Vue.use(VModal);
Vue.use(VueSpinners)

new Vue({
    router,
    render: h => h(App),
    store
}).$mount('#app');
