import Vue from 'vue';
import VueRouter from "vue-router";

import Home from "./modules/Shared/Home";
import Purchases from "./modules/Purchases/Purchases";
import ListPurchases from "./modules/Purchases/Pages/List";
import DetailPurchases from "./modules/Purchases/Pages/Detail";
import RegisterPurchases from "./modules/Purchases/Pages/Register";

import Products from "./modules/Products/Products";
import ListProducts from "./modules/Products/Pages/List";

import Sales from "./modules/Sales/Sales";
import RegisterSales from "./modules/Sales/Pages/Register";
import ListSales from "./modules/Sales/Pages/List";
import Configuration from "./modules/Configuration/Configuration";
import Users from "./modules/Configuration/Pages/Users";
import Facturacion from "./modules/Configuration/Pages/Facturacion";

Vue.use(VueRouter);

export default new VueRouter({
    //mode: 'history',
    routes: [
        {
            path: '/',
            redirect: '/home'
        },
        {
            path: '/home',
            component: Home
        },
        {
            path: '/compras',
            component: Purchases,
            children: [
                {
                    path: '',
                    redirect: 'listar'
                },
                {
                    path: 'listar',
                    component: ListPurchases
                },
                {
                    path: 'detalle/:id',
                    component: DetailPurchases
                },
                {
                    path: 'registrar',
                    component: RegisterPurchases
                }
            ]
        },
        {
            path: '/productos',
            component: Products,
            children: [
                {
                    path: '',
                    redirect: 'listar'
                },
                {
                    path: 'listar',
                    component: ListProducts
                }
            ]
        },
        {
            path: '/ventas',
            component: Sales,
            children: [
                {
                    path: '',
                    redirect: 'listar'
                },
                {
                    path: 'listar',
                    component: ListSales
                },
                {
                    path: 'registrar',
                    component: RegisterSales
                }
            ]
        },
        {
            path: '/configuracion',
            component: Configuration,
            children: [
                {
                    path: '',
                    redirect: 'usuarios'
                },
                {
                    path: 'usuarios',
                    component: Users
                },
                {
                    path: 'facturacion',
                    component: Facturacion
                }
            ]
        }
    ],
    linkExactActiveClass: 'is-active'
});
