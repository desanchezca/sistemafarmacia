<?php


Auth::routes();

Route::get('/', 'HomeController@index');

Route::group(['prefix' => 'reportes'], function() {
    Route::get('/productos', 'Reports\ProductController@report');
    Route::get('/compras', 'Reports\PurchaseDocumentController@report');
    Route::get('/ventas', 'Reports\SaleDocumentController@report');
    Route::get('/vendedores', 'Reports\SellerController@report');
});

