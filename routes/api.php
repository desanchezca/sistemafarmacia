<?php


/*Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'AuthPassportController@login');
    Route::group(['middleware' => 'auth'], function() {
        Route::post('signup', 'AuthPassportController@signup');
        Route::get('logout', 'AuthPassportController@logout');
        Route::get('user', 'AuthPassportController@user');
    });
});*/

Route::apiResource('users', 'API\UserController');
Route::apiResource('billing-codes', 'API\BillingCodeController');
Route::apiResource('owner-documents', 'API\OwnerDocumentController');
Route::apiResource('categories', 'API\CategoryController');
Route::apiResource('brands', 'API\BrandController');
Route::apiResource('branches', 'API\BranchController');
Route::apiResource('laboratories', 'API\LaboratoryController');
Route::apiResource('measure-units', 'API\MeasureUnitController');
Route::apiResource('products', 'API\ProductController');
Route::apiResource('purchase-documents', 'API\PurchaseDocumentController');
Route::apiResource('purchase-documents.details', 'API\PurchaseDocumentDetailController');
Route::apiResource('sale-documents', 'API\SaleDocumentController');
// Route::apiResource('sale-documents.details', 'API\SaleDocumentDetailController');
