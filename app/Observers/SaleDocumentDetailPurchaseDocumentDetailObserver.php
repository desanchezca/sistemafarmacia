<?php

namespace App\Observers;

use App\Models\SaleDocumentDetailPurchaseDocumentDetail;

class SaleDocumentDetailPurchaseDocumentDetailObserver
{
    public function created(SaleDocumentDetailPurchaseDocumentDetail $saleDocumentDetailPurchaseDocumentDetail)
    {
        $sold_units = $saleDocumentDetailPurchaseDocumentDetail->saleDocumentDetail->product->sold_units;

        $saleDocumentDetailPurchaseDocumentDetail->saleDocumentDetail->product->update([
            'sold_units' => $sold_units + $saleDocumentDetailPurchaseDocumentDetail->quantity
        ]);
    }

    /**
     * Handle the sale document detail purchase document detail "updated" event.
     *
     * @param  \App\Models\SaleDocumentDetailPurchaseDocumentDetail  $saleDocumentDetailPurchaseDocumentDetail
     * @return void
     */
    public function updated(SaleDocumentDetailPurchaseDocumentDetail $saleDocumentDetailPurchaseDocumentDetail)
    {
        //
    }

    /**
     * Handle the sale document detail purchase document detail "deleted" event.
     *
     * @param  \App\Models\SaleDocumentDetailPurchaseDocumentDetail  $saleDocumentDetailPurchaseDocumentDetail
     * @return void
     */
    public function deleted(SaleDocumentDetailPurchaseDocumentDetail $saleDocumentDetailPurchaseDocumentDetail)
    {
        //
    }

    /**
     * Handle the sale document detail purchase document detail "restored" event.
     *
     * @param  \App\Models\SaleDocumentDetailPurchaseDocumentDetail  $saleDocumentDetailPurchaseDocumentDetail
     * @return void
     */
    public function restored(SaleDocumentDetailPurchaseDocumentDetail $saleDocumentDetailPurchaseDocumentDetail)
    {
        //
    }

    /**
     * Handle the sale document detail purchase document detail "force deleted" event.
     *
     * @param  \App\Models\SaleDocumentDetailPurchaseDocumentDetail  $saleDocumentDetailPurchaseDocumentDetail
     * @return void
     */
    public function forceDeleted(SaleDocumentDetailPurchaseDocumentDetail $saleDocumentDetailPurchaseDocumentDetail)
    {
        //
    }
}
