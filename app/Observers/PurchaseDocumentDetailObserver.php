<?php

namespace App\Observers;

use App\Models\PurchaseDocumentDetail;

class PurchaseDocumentDetailObserver
{
    public function created(PurchaseDocumentDetail $purchaseDocumentDetail)
    {
        $quantity_purchased = $purchaseDocumentDetail->quantity;
        $unit_price = $purchaseDocumentDetail->unit_price;

        $current_quantity = $purchaseDocumentDetail->product->purchased_units;
        $purchaseDocumentDetail->product->update(['purchased_units' => $current_quantity + $quantity_purchased]);

        $document_total = $purchaseDocumentDetail->purchaseDocument->total;
        $purchaseDocumentDetail->purchaseDocument->update([
            'total' => $document_total + ($quantity_purchased * $unit_price)
        ]);
    }

    public function updated(PurchaseDocumentDetail $purchaseDocumentDetail)
    {
        //
    }

    public function deleted(PurchaseDocumentDetail $purchaseDocumentDetail)
    {
        //
    }

    public function restored(PurchaseDocumentDetail $purchaseDocumentDetail)
    {
        //
    }

    public function forceDeleted(PurchaseDocumentDetail $purchaseDocumentDetail)
    {
        //
    }
}
