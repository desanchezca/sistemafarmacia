<?php

namespace App\Http\Controllers\API;

use App\Http\Builders\PurchaseDocumentBuilder;
use App\Http\Controllers\Controller;
use App\Http\Helpers\HandleHttpError;
use App\Http\Resources\PurchaseDocumentResource;
use App\Models\PurchaseDocument;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class PurchaseDocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $purchases = PurchaseDocument::where('branch_id', auth()->user()->branch_id)->get();
        return (PurchaseDocumentResource::collection($purchases))->response()->setStatusCode(200);
    }

    public function store(Request $request)
    {
        $code = $request->post('code');
        $provider_id = $request->post('provider_id');
        $builder = new PurchaseDocumentBuilder();
        $purchase = $builder->provider($request)
                ->code($code)
                ->defaultState()
                ->defaultType()
                ->defaultCurrency()
                ->withoutCommentary()
                ->savePurchaseDocument()
                ->saveManyDetailsFromRequest($request->post('details'))
                ->getModel();
        return (new PurchaseDocumentResource($purchase))->response()->setStatusCode(200);
    }

    public function show($id)
    {
        try {
            $purchase_document = PurchaseDocument::where('id', $id)->first();
            return (new PurchaseDocumentResource($purchase_document))->response()->setStatusCode(200);
        } catch (ModelNotFoundException $e)
        {
            return HandleHttpError::badRequest();
        }
    }

    public function update(Request $request, $id)
    {
        // TODO: Implementar
    }

    public function destroy($id)
    {
        //
    }
}
