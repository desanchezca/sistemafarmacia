<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Helpers\AuthData;
use App\Http\Helpers\HandleHttpError;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\ProductResource;
use App\Http\Resources\ProductResourceCollection;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $products = Product::all();
        return (new ProductResourceCollection($products))->response()->setStatusCode(200);
    }

    public function store(ProductRequest $request)
    {
        $product = new Product();
        $data = $request->only($product->getFillable());
        $auth_data = new AuthData();
        $product->branch_id = $auth_data->getBranchIdThroughUserAuthenticated();
        $product->fill($data)->save();
        return (new ProductResource($product))->response()->setStatusCode(201);
    }

    public function show($id)
    {
        try {
            $product = Product::findOrFail($id);
            return (new ProductResource($product))->response()->setStatusCode(200);
        } catch (Exception $e) {
            return HandleHttpError::badRequest();
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $product = Product::findOrFail($id);
            $data = $request->only($product->getFillable());
            return (new ProductResource($product->fill($data)->update()))->response()->setStatusCode(201);
        } catch (Exception $e) {
            return HandleHttpError::badRequest();
        }
    }

    public function destroy(Product $product)
    {
        return HandleHttpError::unauthorized();
    }
}
