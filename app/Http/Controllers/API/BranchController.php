<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Helpers\HandleHttpError;
use App\Http\Requests\BranchRequest;
use App\Http\Resources\BranchResource;
use App\Models\Branch;
use Exception;

class BranchController extends Controller
{
    public function index()
    {
        return (BranchResource::collection(Branch::all()))->response()->setStatusCode(200);
    }

    public function store(BranchRequest $request)
    {
        $branch = new Branch();
        $data = $request->only($branch->getFillable());
        return (new BranchResource($branch->fill($data)->save()))->response()->setStatusCode(201);
    }

    public function show($id)
    {
        try {
            $branch = Branch::findOrFail($id);
            return (new BranchResource($branch))->response()->setStatusCode(200);
        } catch (Exception $e) {
            return HandleHttpError::badRequest();
        }
    }

    public function update(BranchRequest $request, $id)
    {
        try {
            $branch = Branch::findOrFail($id);
            $data = $request->only($branch->getFillable());
            return (new BranchResource($branch->fill($data)->update()))->response()->setStatusCode(201);
        } catch (Exception $e) {
            return HandleHttpError::badRequest();
        }
    }

    public function destroy($id)
    {
        return HandleHttpError::unauthorized();
    }
}
