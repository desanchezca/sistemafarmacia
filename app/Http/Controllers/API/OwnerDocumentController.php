<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Helpers\HandleHttpError;
use App\Http\Requests\OwnerDocumentRequest;
use App\Http\Resources\OwnerDocumentResource;
use App\Models\OwnerDocument;
use Exception;
use Illuminate\Http\Request;

class OwnerDocumentController extends Controller
{
    public function index()
    {
        return (OwnerDocumentResource::collection(OwnerDocument::all()))->response()->setStatusCode(200);
    }

    public function store(OwnerDocumentRequest $request)
    {
        $owner_document = new OwnerDocument();
        $data = $request->only($owner_document->getFillable());
        $owner_document->fill($data);
        return (new OwnerDocumentResource(OwnerDocument::firstOrCreate($owner_document->toArray())))
            ->response()->setStatusCode(201);
    }

    public function show($id)
    {
        try {
            $owner_document = OwnerDocument::findOrFail($id);
            return (new OwnerDocumentResource($owner_document))->response()->setStatusCode(200);
        } catch (Exception $e) {
            return HandleHttpError::badRequest();
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $owner_document = OwnerDocument::findOrFail($id);
            $data = $request->only($owner_document->getFillable());
            return (new OwnerDocumentResource($owner_document->fill($data)->update()))->response()->setStatusCode(201);
        } catch (Exception $e) {
            return HandleHttpError::badRequest();
        }
    }

    public function destroy($id)
    {
        return HandleHttpError::unauthorized();
    }
}
