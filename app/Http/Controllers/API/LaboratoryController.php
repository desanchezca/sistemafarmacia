<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Helpers\HandleHttpError;
use App\Models\Laboratory;
use Illuminate\Http\Request;

class LaboratoryController extends Controller
{
    public function index()
    {
        return response()->json(Laboratory::all()->pluck('name'), 200);
    }

    public function store(Request $request)
    {
        return HandleHttpError::unauthorized();
    }

    public function show($id)
    {
        return HandleHttpError::unauthorized();
    }

    public function update(Request $request, $id)
    {
        return HandleHttpError::unauthorized();
    }

    public function destroy($id)
    {
        return HandleHttpError::unauthorized();
    }
}
