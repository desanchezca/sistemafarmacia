<?php

namespace App\Http\Controllers\API;

use App\Http\Builders\SaleDocumentBuilder;
use App\Http\Controllers\Controller;
use App\Models\SaleDocument;
use Illuminate\Http\Request;

class SaleDocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $sale_documents = SaleDocument::all();

        return response()->json([
            'data' => $sale_documents
        ])->setStatusCode(200);
    }

    public function store(Request $request)
    {

        try {
            $client_id = $request->post('client_id');
            $sale_document_builder = new SaleDocumentBuilder();
            $sale_document = $sale_document_builder
                ->client($request)
                ->seller()
                ->billingCode()
                ->withoutCommentary()
                ->completedState()
                ->defaultCurrency()
                ->saveSaleDocument()
                ->saveManyDetailsFromRequest($request->post('details'))
                ->getModel();

            return response()->json(['data' => $sale_document])->setStatusCode(201);
        } catch (Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ])->setStatusCode(400);
        }

    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
