<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Helpers\HandleHttpError;
use App\Models\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    public function index()
    {
        return response()->json(Brand::all()->pluck('name'), 200);
    }

    public function store(Request $request)
    {
        return HandleHttpError::unauthorized();
    }

    public function show($id)
    {
        return HandleHttpError::unauthorized();
    }

    public function update(Request $request, $id)
    {
        return HandleHttpError::unauthorized();
    }

    public function destroy($id)
    {
        return HandleHttpError::unauthorized();
    }
}
