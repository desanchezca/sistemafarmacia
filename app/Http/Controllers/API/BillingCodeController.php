<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\BillingCode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BillingCodeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $billing_codes = BillingCode::where('branch_id', Auth::user()->branch_id)->get();
        return response()->json([
            'data' => $billing_codes
        ])->setStatusCode(200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $branch_id = Auth::user()->branch_id;
        $initial = $request->post('initial');
        $increment = $request->post('increment');
        $type = $request->post('type');

        return response()->json([
            'data' => BillingCode::create([
                'branch_id' => $branch_id,
                'initial' => $initial,
                'increment' => $increment,
                'type' => $type
            ])
        ])->setStatusCode(201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
