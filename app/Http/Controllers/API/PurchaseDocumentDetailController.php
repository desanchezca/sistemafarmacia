<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Helpers\HandleHttpError;
use App\Http\Resources\PurchaseDocumentDetailResource;
use App\Models\PurchaseDocument;
use Illuminate\Http\Request;

class PurchaseDocumentDetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(PurchaseDocument $purchase_document)
    {
        return (PurchaseDocumentDetailResource::collection($purchase_document->details))->response()->setStatusCode(200);
    }

    public function store(PurchaseDocument $purchase_document, Request $request)
    {
        return HandleHttpError::unauthorized();
    }

    public function show(PurchaseDocument $purchase_document, $id)
    {
        return HandleHttpError::unauthorized();
    }

    public function update(PurchaseDocument $purchase_document, Request $request, $id)
    {
        return HandleHttpError::unauthorized();
    }

    public function destroy(PurchaseDocument $purchase_document, $id)
    {
        return HandleHttpError::unauthorized();
    }
}
