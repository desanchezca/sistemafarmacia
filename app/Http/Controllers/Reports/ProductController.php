<?php

namespace App\Http\Controllers\Reports;

use App\Models\Product;
use PDF;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function report()
    {
        $products = Product::all();

        $pdf = PDF::loadView('pdf.product', [
            'title' => 'REPORTE DE PRODUCTOS',
            'products' => $products
        ]);

        return $pdf->stream();
    }
}
