<?php

namespace App\Http\Controllers\Reports;

use PDF;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SellerController extends Controller
{
    public function report()
    {
        $pdf = PDF::loadView('pdf.sellers', [
            'title' => 'REPORTE DE VENDEDORES'
        ]);
        return $pdf->stream();
    }
}
