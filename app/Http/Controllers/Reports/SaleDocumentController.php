<?php

namespace App\Http\Controllers\Reports;

use App\Http\Builders\ReportBuilder;
use App\Models\SaleDocument;
use PDF;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SaleDocumentController extends Controller
{
    public function report(Request $request)
    {
        $report_builder = new ReportBuilder(SaleDocument::all());
        $sales = $report_builder->setFromDate($request)->setToDate($request)->getFiltered();
        $pdf = PDF::loadView('pdf.generalsales', [
            'title' => 'REPORTE DE VENTAS',
            'from' => $report_builder->getFromDateTime(),
            'to' => $report_builder->getToDateTime(),
            'sales' => $sales
        ]);
        return $pdf->stream();
    }
}
