<?php

namespace App\Http\Controllers\Reports;

use App\Http\Builders\ReportBuilder;
use App\Models\PurchaseDocument;
use PDF;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PurchaseDocumentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function report(Request $request)
    {
        $report_builder = new ReportBuilder(PurchaseDocument::all());
        $purchases = $report_builder->setFromDate($request)->setToDate($request)->getFiltered();
        $pdf = PDF::loadView('pdf.generalpurchases', [
            'title' => 'REPORTE DE COMPRAS',
            'from' => $report_builder->getFromDateTime(),
            'to' => $report_builder->getToDateTime(),
            'purchases' => $purchases
        ]);
        return $pdf->download('reporte_de_compras.pdf');
    }
}
