<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'code' => ['required', 'string', 'unique:products,code'],
                    'category' => ['string'],
                    'brand' => ['string'],
                    'laboratory' => ['string'],
                    'measure_unit' => ['required', 'string'],
                    'name' => ['required', 'string'],
                    'composition' => ['string'],
                    'description' => ['required', 'string']
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'category' => ['string'],
                    'brand' => ['string'],
                    'laboratory' => ['string'],
                    'measure_unit' => ['required', 'string'],
                    'name' => ['required', 'string'],
                    'composition' => ['string'],
                    'description' => ['required', 'string']
                ];
            case 'GET':
            case 'DELETE':
            default:
                return [];
        }
    }
}
