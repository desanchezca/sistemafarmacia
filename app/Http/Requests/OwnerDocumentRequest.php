<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OwnerDocumentRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'identity_document' => ['required', 'string'],
                    'name' => ['required', 'string'],
                    'address' => ['string'],
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'name' => ['required', 'string'],
                    'address' => ['required', 'string']
                ];
            case 'GET':
            case 'DELETE':
            default:
                return [];
        }
    }
}
