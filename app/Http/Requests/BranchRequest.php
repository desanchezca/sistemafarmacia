<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BranchRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'ruc' => ['required', 'string', 'size:11', 'regex:/^[0-9]+$/', 'unique:branches,ruc'],
                    'name' => ['required', 'string'],
                    'address' => ['required', 'string'],
                    'email' => ['email'],
                    'phone' => ['string'],
                    'website' => ['string'],
                ];
            case 'PUT':
            case 'PATCH':
                return [
                    'name' => ['required', 'string'],
                    'address' => ['required', 'string'],
                    'email' => ['email'],
                    'phone' => ['string'],
                    'website' => ['string']
                ];
            case 'GET':
            case 'DELETE':
            default:
                return [];
        }
    }
}
