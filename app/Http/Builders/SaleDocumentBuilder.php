<?php


namespace App\Http\Builders;


use App\Http\Helpers\AuthData;
use App\Models\BillingCode;
use App\Models\OwnerDocument;
use App\Models\SaleDocument;
use http\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SaleDocumentBuilder
{
    private $sale_document;
    private $auth_data;

    private const LENGHT_FOR_FACTURA = 11;
    private const LENGHT_FOR_BOLETA = 8;

    public function __construct()
    {
        $this->auth_data = new AuthData();
        $this->sale_document = new SaleDocument();
        $this->sale_document->branch_id = $this->auth_data->getBranchIdThroughUserAuthenticated();
        $this->sale_document->seller_id = $this->auth_data->getCurrentUserId();
    }

    public function client(Request $request)
    {
        $client = OwnerDocument::firstOrCreate([
            'identity_document' => $request->post('client')['identity_document'],
            'name' => $request->post('client')['name'],
            'address' => $request->post('client')['address']
        ]);

        $this->sale_document->client_id = $client->id;
        $this->sale_document->client_name = $client->name;
        $this->sale_document->client_address = $client->address;
        $this->sale_document->client_document = $client->identity_document;
        return $this;
    }

    public function clientOld($client_id)
    {
        $client = OwnerDocument::where('id', $client_id)->first();
        $this->sale_document->client_id = $client->id;
        $this->sale_document->client_name = $client->name;
        $this->sale_document->client_address = $client->address;
        $this->sale_document->client_document = $client->identity_document;
        return $this;
    }

    public function seller()
    {
        $this->sale_document->seller_id = Auth::user()->id;
        $this->sale_document->seller = Auth::user()->name;
        return $this;
    }

    public function billingCode()
    {
        $auth_data = new AuthData();

        if (strlen($this->sale_document->client_document) === self::LENGHT_FOR_FACTURA) {
            $latest_document = BillingCode::where([
                ['branch_id', '=', $auth_data->getBranchIdThroughUserAuthenticated()],
                ['type', '=', 'FACTURA']
            ])->latest()->first();
            $this->sale_document->type = "FACTURA";
        } else {
            $latest_document = BillingCode::where([
                ['branch_id', '=', $auth_data->getBranchIdThroughUserAuthenticated()],
                ['type', '=', 'BOLETA']
            ])->latest()->first();

            $this->sale_document->type = "BOLETA";
        }

        $new_billing_document = new BillingCode();
        $new_billing_document->branch_id = Auth::user()->branch_id;
        $new_billing_document->increment = $latest_document->increment + 1 ;
        $new_billing_document->initial = $latest_document->initial;
        $new_billing_document->type = $latest_document->type;
        $new_billing_document->save();

        $this->sale_document->billing_code_id = $new_billing_document->id;
        $this->sale_document->code = $this->getCodeForSale($new_billing_document);

        return $this;
    }

    private function getCodeForSale(BillingCode $billing_code)
    {
        return $billing_code->initial.'-'.str_pad($billing_code->increment, 8, '0', STR_PAD_LEFT);
    }

    public function withoutCommentary()
    {
        $this->sale_document->commentary = '';
        return $this;
    }

    public function completedState()
    {
        $this->sale_document->state = 'COMPLETADO';
        return $this;
    }

    public function defaultCurrency()
    {
        $this->sale_document->currency = "PEN";
        return $this;
    }

    public function saveSaleDocument()
    {
        $this->sale_document->save();
        return $this;
    }

    public function getModel()
    {
        $this->sale_document->refresh();
        return $this->sale_document;
    }


    public function saveManyDetailsFromRequest($details)
    {
        foreach ($details as $key=>$detail)
        {
            $builder = new SaleDocumentDetailBuilder($this->sale_document);
            $sale_detail = $builder->searchProduct($detail['id'])
                ->setProductAttributes()
                ->item($key + 1)
                ->quantity($detail['quantity'])
                ->unitPrice($detail['unit_price'])
                ->purchaseDocuments();
        }

        return $this;
    }
}
