<?php


namespace App\Http\Builders;
use Illuminate\Http\Request;

class ReportBuilder
{
    private $collection;

    private $from_year;
    private $from_month;
    private $from_day;

    private $to_year;
    private $to_month;
    private $to_day;

    public function __construct($collection)
    {
        $this->collection = $collection;
    }

    public function setFromDate(Request $request)
    {
        if ($request->get('fromYear') && $request->get('fromMonth') && $request->get('fromDay'))
        {
            $this->from_year = $request->get('fromYear');
            $this->from_month = $request->get('fromMonth');
            $this->from_day = $request->get('fromDay');
        }
        else
        {
            $this->from_day = date('j');
            $this->from_month = date('n');
            $this->from_year = date('Y');
        }

        return $this;
    }

    public function setToDate(Request $request)
    {
        if ($request->get('toYear') && $request->get('toMonth') && $request->get('toDay'))
        {
            $this->to_year = $request->get('toYear');
            $this->to_month = $request->get('toMonth');
            $this->to_day = $request->get('toDay');
        }
        else
        {
            $this->to_day = date('j');
            $this->to_month = date('n');
            $this->to_year = date('Y');
        }

        return $this;
    }

    public function getFiltered()
    {
        $from = $this->getFromDateTime();
        $to = $this->getToDateTime();
        return $this->collection->where('created_at', '>', $from)->where('created_at', '<', $to);
    }

    public function getFromDateTime()
    {
        return new \DateTime($this->from_year.'/'.$this->from_month.'/'.$this->from_day.' 00:00:00');
    }

    public function getToDateTime()
    {
        return new \DateTime($this->to_year.'/'.$this->to_month.'/'.$this->to_day.' 23:59:59');
    }
}
