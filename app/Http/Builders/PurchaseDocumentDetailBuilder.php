<?php


namespace App\Http\Builders;


use App\Models\Product;
use App\Models\PurchaseDocument;
use App\Models\PurchaseDocumentDetail;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;

class PurchaseDocumentDetailBuilder
{
    private $document_detail;
    private $product;

    public function __construct(PurchaseDocument $purchase_document)
    {
        $this->document_detail = new PurchaseDocumentDetail();
        $this->document_detail->purchase_document_id = $purchase_document->id;
        $this->document_detail->document_code = $purchase_document->code;
    }

    public function setProduct($detail)
    {
        if ($detail['id'] === 0)
            $this->product = $this->getProductWhenIdIsZero($detail);
        else
            $this->product = $this->getProductFromDatabase($detail);
        return $this->setProductAttributes();
    }

    private function getProductWhenIdIsZero($detail)
    {
        return Product::create([
            'branch_id' => Auth::user()->branch_id,
            'code' => $detail['code'],
            'brand' => $detail['brand'],
            'laboratory' => $detail['laboratory'],
            'measure_unit' => $detail['measure_unit'],
            'category' => $detail['category'],
            'name' => $detail['name'],
            'composition' => $detail['composition'],
            'description' => $detail['description']
        ]);
    }

    public function getProductFromDatabase($detail)
    {
        return Product::where('id', $detail['id'])->first();
    }

    public function searchProduct($id)
    {
        $this->product = Product::where('id', $id)->first();
        return $this;
    }

    public function setProductAttributes()
    {
        $this->document_detail->product_id = $this->product->id;
        $this->document_detail->product_code = $this->product->code;
        $this->document_detail->product_name = $this->product->name;
        $this->document_detail->product_measure_unit = $this->product->measure_unit;
        $this->document_detail->product_brand = $this->product->brand;
        return $this;
    }

    public function item($value)
    {
        $this->document_detail->item = $value;
        return $this;
    }

    public function quantity($value)
    {
        $this->document_detail->quantity = $value;
        return $this;
    }

    public function unitPrice($value)
    {
        $this->document_detail->unit_price = $value;
        return $this;
    }

    public function build()
    {
        $this->document_detail->save();
        return $this->document_detail;
    }
}
