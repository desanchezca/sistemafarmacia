<?php


namespace App\Http\Builders;


use App\Http\Helpers\AuthData;
use App\Models\Product;
use App\Models\PurchaseDocumentDetail;
use App\Models\SaleDocument;
use App\Models\SaleDocumentDetail;
use App\Models\SaleDocumentDetailPurchaseDocumentDetail;

class SaleDocumentDetailBuilder
{
    private $document_detail;
    private $product;

    public function __construct(SaleDocument $sale_document)
    {
        $this->document_detail = new SaleDocumentDetail();
        $this->document_detail->sale_document_id = $sale_document->id;
        $this->document_detail->sale_document_code = $sale_document->code;
    }

    public function searchProduct($id)
    {
        $this->product = Product::where('id', $id)->first();
        return $this;
    }

    public function setProductAttributes()
    {
        $this->document_detail->product_id = $this->product->id;
        $this->document_detail->product_code = $this->product->code;
        $this->document_detail->product_name = $this->product->name;
        $this->document_detail->product_brand = $this->product->brand;
        return $this;
    }

    public function item($value)
    {
        $this->document_detail->sale_document_item = $value;
        return $this;
    }

    public function quantity($value)
    {
        $this->document_detail->quantity = $value;
        return $this;
    }

    public function unitPrice($value)
    {
        $this->document_detail->unit_price = $value;
        return $this;
    }

    public function purchaseDocuments()
    {
        $this->document_detail->save();
        $purchase_document_details = $this->getPurchaseDocumentDetails();
        $quantity_required = $this->document_detail->quantity;
        $index = 0;
        do {

            $quantity = 0;

            if ($purchase_document_details[$index]->current_quantity > $quantity_required || $purchase_document_details[$index]->current_quantity <= $quantity_required && $quantity_required > 0) {
                if ($purchase_document_details[$index]->current_quantity > $quantity_required) {
                    $quantity = $quantity_required;
                    $quantity_required = 0;
                    $purchase_document_details[$index]->current_quantity -= $quantity_required;
                }
                else {
                    $quantity = $purchase_document_details[$index]->current_quantity;
                    $quantity_required -= $purchase_document_details[$index]->current_quantity;
                    $purchase_document_details[$index]->current_quantity = 0;
                }

                $purchase_document_details[$index]->update();

                $sale_document_detail_purchase_document_detail = new SaleDocumentDetailPurchaseDocumentDetail();
                $sale_document_detail_purchase_document_detail->purchase_document_detail_id = $purchase_document_details[$index]->id;
                $sale_document_detail_purchase_document_detail->sale_document_detail_id = $this->document_detail->id;
                $sale_document_detail_purchase_document_detail->quantity = $quantity;
                $sale_document_detail_purchase_document_detail->save();
            }

            $index++;
        } while ($quantity_required > 0 || $index < count($purchase_document_details));
    }

    private function getPurchaseDocumentDetails()
    {
        $auth_data = new AuthData();
        return  $purchase_document_details = PurchaseDocumentDetail::where([
                    ['purchase_documents.branch_id', $auth_data->getBranchIdThroughUserAuthenticated()],
                    ['purchase_document_details.product_id', $this->product->id],
                    ['purchase_document_details.current_quantity', '>', 0],
                ])
                ->join('purchase_documents', 'purchase_document_id', '=', 'purchase_documents.id')
                ->orderBy('purchase_document_details.created_at', 'ASC')
                ->get();
    }

    public function build()
    {
        return $this->document_detail;
    }
}
