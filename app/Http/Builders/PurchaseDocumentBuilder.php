<?php


namespace App\Http\Builders;

use App\Http\Helpers\AuthData;
use App\Models\OwnerDocument;
use App\Models\Product;
use App\Models\PurchaseDocument;
use Illuminate\Http\Request;

class PurchaseDocumentBuilder
{
    private $purchase_document;
    private $auth_data;

    public function __construct()
    {
        $this->auth_data = new AuthData();
        $this->purchase_document = new PurchaseDocument();
        $this->purchase_document->branch_id = $this->auth_data->getBranchIdThroughUserAuthenticated();
        $this->purchase_document->user_id = $this->auth_data->getCurrentUserId();
    }

    public function provider(Request $request)
    {
        $provider = OwnerDocument::firstOrCreate([
            'identity_document' => $request->post('provider')['identity_document'],
            'name' => $request->post('provider')['name'],
            'address' => $request->post('provider')['address']
        ]);

        $this->purchase_document->provider_id = $provider->id;
        $this->purchase_document->provider_name = $provider->name;
        $this->purchase_document->provider_identity_document = $provider->identity_document;
        $this->purchase_document->provider_address = $provider->address;
        return $this;
    }

    public function setProviderById($id) : PurchaseDocumentBuilder
    {
        $provider = OwnerDocument::where('id', $id)->first();
        $this->purchase_document->provider_id = $id;
        $this->purchase_document->provider_name = $provider->name;
        $this->purchase_document->provider_identity_document = $provider->identity_document;
        $this->purchase_document->provider_address = $provider->address;
        return $this;
    }

    public function code($code) : PurchaseDocumentBuilder
    {
        $this->purchase_document->code = $code;
        return $this;
    }

    public function defaultState() : PurchaseDocumentBuilder
    {
        $this->purchase_document->state = "COMPLETADO";
        return $this;
    }

    public function defaultType() : PurchaseDocumentBuilder
    {
        $this->purchase_document->type = "FACTURA";
        return $this;
    }

    public function defaultCurrency() : PurchaseDocumentBuilder
    {
        $this->purchase_document->currency = "PEN";
        return $this;
    }

    public function withoutCommentary() : PurchaseDocumentBuilder
    {
        $this->purchase_document->commentary = "-";
        return $this;
    }

    public function savePurchaseDocument()
    {
        $this->purchase_document->save();
        return $this;
    }

    public function getModel() : PurchaseDocument
    {
        $this->purchase_document->refresh();
        return $this->purchase_document;
    }

    public function saveManyDetailsFromRequest($details)
    {
        foreach ($details as $key=>$detail)
        {
            $builder = new PurchaseDocumentDetailBuilder($this->purchase_document);
            $purchase_detail = $builder->setProduct($detail)
                                ->item($key + 1)
                                ->quantity($detail['quantity'])
                                ->unitPrice($detail['unit_price'])
                                ->build();
        }
        return $this;
    }
}
