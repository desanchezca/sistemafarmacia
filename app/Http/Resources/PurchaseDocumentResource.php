<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PurchaseDocumentResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'provider_name' => $this->provider_name,
            'provider_identity_document' => $this->provider_identity_document,
            'provider_address' => $this->provider_address,
            'code' => $this->code,
            'type' => $this->type,
            'currency' => $this->currency,
            'state' => $this->state,
            'date' => date('d/m/Y', strtotime($this->created_at)),
            'details' => PurchaseDocumentDetailResource::collection($this->details)
        ];
    }
}
