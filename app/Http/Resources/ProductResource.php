<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class ProductResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'category' => $this->category,
            'brand' => $this->brand,
            'laboratory' => $this->laboratory,
            'measure_unit' => $this->measure_unit,
            'name' => $this->name,
            'composition' => $this->composition,
            'description' => $this->description,
            'in_stock' => $this->in_stock
        ];
    }
}
