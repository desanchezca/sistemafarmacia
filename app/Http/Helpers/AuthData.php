<?php

namespace App\Http\Helpers;

class AuthData
{
    public function getCurrentUserId()
    {
        return auth()->user()->id;
       // return 1;
    }

    public function getBranchIdThroughUserAuthenticated()
    {
        return auth()->user()->branch_id;
        //return 1;
    }
}
