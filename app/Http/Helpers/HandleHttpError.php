<?php


namespace App\Http\Helpers;


class HandleHttpError
{
    public static function badRequest()
    {
        return response()->json(['error' => 400, 'message' => 'Not found'], 400);
    }

    public static function unauthorized()
    {
        return response()->json(['error' => 401, 'message' => 'Unauthorized'], 401);
    }
}
