<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Passport\Passport;

class AppServiceProvider extends ServiceProvider
{

    public function register()
    {
        Passport::ignoreMigrations();
    }

    public function boot()
    {
        \App\Models\PurchaseDocumentDetail::observe(\App\Observers\PurchaseDocumentDetailObserver::class);
        \App\Models\SaleDocumentDetailPurchaseDocumentDetail::observe(\App\Observers\SaleDocumentDetailPurchaseDocumentDetailObserver::class);
    }
}
