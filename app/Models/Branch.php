<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Branch
 * @package App\Models
 *
 * @property string ruc
 * @property string name
 * @property string address
 * @property string email
 * @property string website
 * @property string phone
 */
class Branch extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'ruc', 'name', 'address', 'email', 'website', 'phone'
    ];

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtoupper($value);
    }

    public function setAddressAttribute($value)
    {
        $this->attributes['address'] = strtoupper($value);
    }

    public function setEmailAttribute($value)
    {
        $this->attributes['email'] = strtolower($value);
    }

    public function setWebsiteAttribute($value)
    {
        $this->attributes['website'] = strtolower($value);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
