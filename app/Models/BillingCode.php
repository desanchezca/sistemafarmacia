<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BillingCode
 * @package App\Models
 *
 * @property integer id
 * @property string initial
 * @property string increment
 * @property string type
 */
class BillingCode extends Model
{
    protected $fillable = ['branch_id', 'initial', 'increment', 'type'];
}
