<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PurchaseDocumentDetailController
 * @package App\Models
 *
 * @property integer purchase_document_id
 * @property integer product_id
 * @property string document_code
 * @property string item
 * @property string product_code
 * @property string product_name
 * @property string product_measure_unit
 * @property string product_brand
 * @property float quantity
 * @property float unit_price
 */
class PurchaseDocumentDetail extends Model
{
    protected $fillable = [
        'purchase_document_id', 'product_id', 'document_code',
        'item', 'product_code', 'product_name', 'product_measure_unit',
        'product_brand', 'quantity', 'current_quantity', 'unit_price'
    ];

    public function setDocumentCodeAttribute($value)
    {
        $this->attributes['document_code'] = strtoupper($value);
    }

    public function setProductNameAttribute($value)
    {
        $this->attributes['product_name'] = strtoupper($value);
    }

    public function setProductMeasureUnitAttribute($value)
    {
        $this->attributes['product_measure_unit'] = strtoupper($value);
    }

    public function setProductBrandAttribute($value)
    {
        $this->attributes['product_brand'] = strtoupper($value);
    }

    public function setQuantityAttribute($value)
    {
        $this->attributes['quantity'] = $value;
        $this->attributes['current_quantity'] = $value;
    }

    public function getSubtotalAttribute()
    {
        return number_format($this->unit_price * $this->quantity, 2);
    }

    public function purchaseDocument()
    {
        return $this->belongsTo(PurchaseDocument::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function saleDocumentDetails()
    {
        return $this->belongsToMany(SaleDocumentDetail::class, 'sale_document_detail_purchase_document_details');
    }
}
