<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaleDocumentDetailPurchaseDocumentDetail extends Model
{
    protected $fillable = [
        'purchase_document_detail_id', 'sale_document_detail_id', 'quantity'
    ];

    public function purchaseDocumentDetail()
    {
        return $this->belongsTo(PurchaseDocumentDetail::class);
    }

    public function saleDocumentDetail()
    {
        return $this->belongsTo(SaleDocumentDetail::class);
    }
}
