<?php

namespace App\Models;

use App\Scopes\CurrentBranchScope;
use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PurchaseDocument
 * @package App\Models
 *
 * @property integer branch_id
 * @property integer user_id
 * @property integer provider_id
 * @property string provider_name
 * @property string provider_identity_document
 * @property string provider_address
 * @property string code
 * @property string type
 * @property string currency
 * @property float total
 * @property string state
 * @property string commentary
 */
class PurchaseDocument extends Model
{
    protected $fillable = [
        'branch_id', 'user_id', 'provider_id', 'provider_name',
        'provider_identity_document', 'provider_address',
        'code', 'type', 'currency', 'total', 'state', 'commentary',
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CurrentBranchScope());
    }

    public function setCodeAttribute($value)
    {
        $this->attributes['code'] = strtoupper($value);
    }

    public function setTypeAttribute($value)
    {
        $this->attributes['type'] = strtoupper($value);
    }

    public function setCommentaryAttribute($value)
    {
        $this->attributes['commentary'] = strtoupper($value);
    }

    public function setStateAttribute($value)
    {
        $this->attributes['state'] = strtoupper($value);
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function provider()
    {
        return $this->belongsTo(OwnerDocument::class);
    }

    public function details()
    {
        return $this->hasMany(PurchaseDocumentDetail::class);
    }
}
