<?php

namespace App\Models;

use App\Scopes\CurrentBranchScope;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 * @package App\Models
 *
 * @property integer id
 * @property integer branch_id
 * @property string code
 * @property string category
 * @property string brand
 * @property string laboratory
 * @property string measure_unit
 * @property string name
 * @property string composition
 * @property string description
 * @property string unit_price
 * @property string purchased_units
 * @property string sold_units
 */
class Product extends Model
{
    protected $fillable = [
        'branch_id', 'code', 'category', 'brand', 'laboratory',
        'measure_unit', 'name', 'composition', 'description',
        'unit_price', 'purchased_units', 'sold_units'
    ];

    protected $hidden = ['branch_id', 'created_at', 'updated_at'];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new CurrentBranchScope());
    }

    public function setCategoryAttribute($value)
    {
        $category = Category::firstOrCreate([
            'name' => strtoupper($value)
        ]);
        $this->attributes['category'] = $category->name;
    }

    public function setBrandAttribute($value)
    {
        $brand = Brand::firstOrCreate([
            'name' => strtoupper($value)
        ]);
        $this->attributes['brand'] = $brand->name;
    }

    public function setLaboratoryAttribute($value)
    {
        $laboratory = Laboratory::firstOrCreate([
            'name' => strtoupper($value)
        ]);
        $this->attributes['laboratory'] = $laboratory->name;
    }

    public function setMeasureUnitAttribute($value)
    {
        $measure_unit = MeasureUnit::firstOrCreate([
            'name' => strtoupper($value)
        ]);
        $this->attributes['measure_unit'] = $measure_unit->name;
    }

    public function setCodeAttribute($value)
    {
        $this->attributes['code'] = strtoupper($value);
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = strtoupper($value);
    }

    public function setCompositionAttribute($value)
    {
        $this->attributes['composition'] = strtoupper($value);
    }

    public function setDescriptionAttribute($value)
    {
        $this->attributes['description'] = strtoupper($value);
    }

    public function getInStockAttribute()
    {
        return $this->purchased_units - $this->sold_units;
    }
}
