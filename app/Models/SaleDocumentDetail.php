<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SaleDocumentDetail
 * @package App\Models
 *
 * @property integer sale_document_id
 * @property integer purchase_document_id
 * @property integer purchase_document_detail_id
 * @property integer product_id
 * @property string sale_document_code
 * @property string sale_document_item
 * @property string purchase_document_code
 * @property string purchase_document_item
 * @property string product_code
 * @property string product_brand
 * @property string quantity
 * @property string unit_price
 */

class SaleDocumentDetail extends Model
{
    protected $fillable = [
        'sale_document_id', 'product_id',
        'sale_document_code', 'sale_document_item',
        'product_code', 'product_name',
        'product_brand', 'quantity', 'unit_price'
    ];

    public function purchaseDocument()
    {
        return $this->belongsTo(PurchaseDocument::class);
    }

    public function purchaseDocumentDetail()
    {
        return $this->belongsTo(PurchaseDocumentDetail::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function purchaseDocumentDetails()
    {
        return $this->belongsToMany(PurchaseDocumentDetail::class, 'sale_document_detail_purchase_document_details');
    }
}
