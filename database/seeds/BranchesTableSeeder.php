<?php

use App\Models\Branch;
use Illuminate\Database\Seeder;

class BranchesTableSeeder extends Seeder
{
    public function run()
    {
        factory(App\Models\Branch::class, 15)->create();
        factory(App\User::class, 15)->create();
        factory(App\Models\OwnerDocument::class, 30)->create();
        /*
        Branch::create([
            'ruc' => '12345678965',
            'name' => 'COMPANIA UNO',
            'address' => 'DIRECCION',
            'email' => 'EMAIL',
            'website' => 'WEBSITE',
            'phone' => 'PHONE',
        ]);*/
    }
}
