<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOwnerDocumentsTable extends Migration
{
    public function up()
    {
        Schema::create('owner_documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('identity_document');
            $table->string('name');
            $table->string('address')->default('-');
            $table->string('phone')->default('-');
        });
    }

    public function down()
    {
        Schema::dropIfExists('owner_documents');
    }
}
