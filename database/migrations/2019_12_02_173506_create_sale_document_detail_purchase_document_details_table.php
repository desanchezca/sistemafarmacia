<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaleDocumentDetailPurchaseDocumentDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_document_detail_purchase_document_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('purchase_document_detail_id');
            $table->unsignedBigInteger('sale_document_detail_id');
            $table->decimal('quantity', 8, 2);
            $table->foreign('purchase_document_detail_id')->references('id')->on('purchase_document_details');
            $table->foreign('sale_document_detail_id')->references('id')->on('sale_document_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sale_document_detail_purchase_document_details');
    }
}
