<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseDocumentDetailsTable extends Migration
{
    public function up()
    {
        Schema::create('purchase_document_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('purchase_document_id');
            $table->unsignedBigInteger('product_id');
            $table->string('document_code');
            $table->integer('item')->unsigned();
            $table->string('product_code');
            $table->string('product_name');
            $table->string('product_measure_unit');
            $table->string('product_brand');
            $table->decimal('quantity', 8, 2);
            $table->decimal('current_quantity', 8, 2);
            $table->decimal('unit_price', 8, 2);
            $table->timestamps();

            $table->foreign('purchase_document_id')->references('id')->on('purchase_documents');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    public function down()
    {
        Schema::dropIfExists('purchase_document_details');
    }
}
