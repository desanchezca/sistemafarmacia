<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingCodesTable extends Migration
{
    public function up()
    {
        Schema::create('billing_codes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('branch_id');
            $table->string('initial');
            $table->integer('increment');
            $table->enum('type', ['BOLETA', 'FACTURA']);
            $table->timestamps();

            $table->foreign('branch_id')->references('id')->on('branches');
        });
    }

    public function down()
    {
        Schema::dropIfExists('billing_codes');
    }
}
