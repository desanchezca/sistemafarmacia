<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaleDocumentDetailsTable extends Migration
{
    public function up()
    {
        Schema::create('sale_document_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('sale_document_id');
            $table->unsignedBigInteger('product_id');
            $table->string('sale_document_code');
            $table->string('sale_document_item');
            $table->string('product_code');
            $table->string('product_name');
            $table->string('product_brand');
            $table->string('quantity');
            $table->string('unit_price');
            $table->timestamps();

            $table->foreign('sale_document_id')->references('id')->on('sale_documents');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    public function down()
    {
        Schema::dropIfExists('sale_document_details');
    }
}
