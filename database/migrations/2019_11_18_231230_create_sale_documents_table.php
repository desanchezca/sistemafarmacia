<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaleDocumentsTable extends Migration
{
    public function up()
    {
        Schema::create('sale_documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('branch_id');
            $table->unsignedBigInteger('seller_id');
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('billing_code_id');
            $table->string('seller');
            $table->string('client_name')->default('-');
            $table->string('client_address')->default('-');
            $table->string('client_document')->default('-');
            $table->string('state')->default('COMPLETADO');
            $table->string('commentary')->default('-');
            $table->string('code')->default('-');
            $table->string('type');
            $table->string('currency')->default('PEN');
            $table->float('subtotal', 8, 2)->default(0);
            $table->float('tax', 8, 2)->default(0);
            $table->float('total', 8, 2)->default(0);

            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('seller_id')->references('id')->on('users');
            $table->foreign('client_id')->references('id')->on('owner_documents');
            $table->foreign('billing_code_id')->references('id')->on('billing_codes');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sale_documents');
    }
}
