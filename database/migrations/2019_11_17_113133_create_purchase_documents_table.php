<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseDocumentsTable extends Migration
{
    public function up()
    {
        Schema::create('purchase_documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('branch_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('provider_id');
            $table->string('provider_name');
            $table->string('provider_identity_document');
            $table->string('provider_address');
            $table->string('code');
            $table->string('type')->default('FACTURA');
            $table->string('currency')->default('PEN');
            $table->string('total')->default(0);
            $table->string('state');
            $table->string('commentary');
            $table->timestamps();

            $table->foreign('branch_id')->references('id')->on('branches');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('provider_id')->references('id')->on('owner_documents');
        });
    }

    public function down()
    {
        Schema::dropIfExists('purchase_documents');
    }
}
