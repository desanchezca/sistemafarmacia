<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Branch;
use Faker\Generator as Faker;

$factory->define(Branch::class, function (Faker $faker) {
    return [
        'ruc' => '12345678'.$faker->randomNumber(3, true),
        'name' => $faker->company,
        'address' => $faker->address,
        'email' => $faker->safeEmail,
        'website' => $faker->url,
        'phone' => $faker->phoneNumber,
    ];
});
