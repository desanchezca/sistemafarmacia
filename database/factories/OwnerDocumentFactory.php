<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OwnerDocument;
use Faker\Generator as Faker;

$factory->define(OwnerDocument::class, function (Faker $faker) {
    return [
        'identity_document' => '12345678'.$faker->randomNumber(3, false),
        'name' => $faker->name,
    ];
});
